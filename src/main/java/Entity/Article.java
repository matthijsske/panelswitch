/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Entity;

/**
 *
 * @author Maurice
 */
public class Article {
   private int articleId;
   private String articleName;

    public Article(int articleId, String articleName) {
        this.articleId = articleId;
        this.articleName = articleName;
    }

    public String getArticleName() {
        return articleName;
    }

    public int getArticleId() {
        return articleId;
    }

}
