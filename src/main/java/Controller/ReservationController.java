package Controller;

import Entity.Article;
import Entity.Customer;
import Entity.Reservation;
import Entity.ReservationList;

import java.time.LocalDateTime;

/**
 * Created by Matthijsske on 9-6-2015.
 */
public class ReservationController {
    private ReservationList reservationList;

    public ReservationController() {

    }

    public void addReservation(LocalDateTime beginDate, LocalDateTime endDate, int customerId, int articleId) {
        Reservation newReservation = new Reservation(5);
        newReservation.setStartDate(beginDate);
        newReservation.setEndDate(endDate);
        newReservation.setArticleId(articleId);
        newReservation.setCustomerId(customerId);
        reservationList.addReservation(newReservation);
    }

    public int getReservationAmount() {
        return 0;
    }

    public int[] getReservations(int customerId) {
        return null;
    }

    public String getReservationInfo(int customerId) {
        return null;
    }
}
