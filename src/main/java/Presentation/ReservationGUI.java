package Presentation;

import Controller.ReservationController;
import Entity.Reservation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Matthijsske on 9-6-2015.
 */
public class ReservationGUI extends JFrame {
    private ReservationController rController;
    public ReservationGUI(ReservationController rController) {
        this.rController = rController;
        this.setSize(400, 300);
        JPanel panel = new ReservationPanel();
        setLayout(new BorderLayout());
        this.setContentPane(panel);
        this.setDefaultCloseOperation(JFrame. EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    private class ReservationPanel extends JPanel {
        private JButton createReservation;
        private JButton overviewReservation;
        private JPanel panelHolder;
        public ReservationPanel() {
            panelHolder = new JPanel();
            createReservation = new JButton("Create Reservation");
            overviewReservation = new JButton("Overview Reservation");
            createReservation.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    panelHolder.removeAll();
                    panelHolder.add(new CreateReservationPanel());
                    panelHolder.revalidate();
                }
            });
            overviewReservation.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    panelHolder.removeAll();
                    panelHolder.add(new OverviewReservationPanel());
                    panelHolder.revalidate();
                }
            });
            JPanel buttonHolder = new JPanel();
            buttonHolder.setLayout(new GridLayout(1,2));
            buttonHolder.add(createReservation);
            buttonHolder.add(overviewReservation);
            add(buttonHolder, BorderLayout.NORTH);
            add(panelHolder, BorderLayout.CENTER);
        }

    }

    private class CreateReservationPanel extends JPanel {
        public CreateReservationPanel() {
            JPanel formHolder = new JPanel();
            formHolder.setLayout(new GridLayout(4, 2));
            formHolder.add(new JLabel("KlantId"));
            JTextField customerId = new JTextField(10);
            formHolder.add(customerId);

            formHolder.add(new JLabel("ArtikelId"));
            JTextField articleId = new JTextField(10);
            formHolder.add(articleId);

            formHolder.add(new JLabel("Begin Datum"));
            JTextField beginDate = new JTextField(10);
            formHolder.add(beginDate);

            formHolder.add(new JLabel("Eind Datum"));
            JTextField endDate = new JTextField(10);
            formHolder.add(endDate);
            add(formHolder);

            add(new JButton("Maak Reservering"));
        }
    }

    private class OverviewReservationPanel extends JPanel {
        public OverviewReservationPanel() {
            add(new JTextField(10));
        }
    }
}
